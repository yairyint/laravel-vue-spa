<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth/login', 'AuthController@login');
Route::post('auth/register', 'AuthController@register');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');
});

Route::group(['middleware' => 'jwt.refresh'], function () {
    Route::get('auth/refresh', 'AuthController@refresh');
});

// Route::get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('expensesAmount', 'API\ExpenseController@getExpenseAmount');
Route::get('incomeAmount', 'API\ExpenseController@getIncomeAmount');

Route::get('expensesRecord', 'API\ExpenseController@getExpenseRecord');
Route::get('incomesRecord', 'API\ExpenseController@getIncomeRecord');

Route::get('expenses/categories', 'API\CategoriesController@getExpenseCategories');

Route::get('income/categories', 'API\CategoriesController@getIncomeCategories');

Route::resource('expenses', 'API\ExpenseController');

Route::resource('categories', 'API\CategoriesController');
