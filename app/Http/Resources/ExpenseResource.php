<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'category_id' => $this->category_id,
            'category_name' => $this->category->name,
            'category_type' => $this->category->type,
            'created_at' => $this->created_at->toFormattedDateString(),
        ];
    }
}
