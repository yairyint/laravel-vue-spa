<?php

namespace App\Http\Controllers\API;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Category::all()->jsonSerialize(), 200);
    }

    public function getExpenseCategories()
    {

        $categories = Category::where('type', 'Expenses')->get();

        return response()->json($categories, 200);
    }

    public function getIncomeCategories()
    {
        $categories = Category::where('type', 'Income')->get();

        return response()->json($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->value,
        ]);
        return response(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = Category::findOrFail($id);
        $categories->update([
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->value,
        ]);
        return response($categories->jsonSerialize(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return response(Category::all()->jsonSerialize(), 200);
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response($category->jsonSerialize(), 200);
    }
}
