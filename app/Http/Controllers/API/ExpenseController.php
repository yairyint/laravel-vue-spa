<?php

namespace App\Http\Controllers\API;

use App\Expense;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ExpenseResource;
use App\Http\Resources\AmountResource;
use Carbon;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amounts = Expense::latest()->whereDate('created_at', Carbon\Carbon::today())->get();

        return ExpenseResource::collection($amounts);
        
        // return new AmountResource(Amount::find(1));
    }
    public function getExpenseAmount()
    {
        $expenses = Expense::where('category_type', '=', 'Expenses')->get();
        return AmountResource::collection($expenses);
    }

    public function getIncomeAmount()
    {
        $incomes = Expense::where('category_type', '=', 'Income')->get();
        return AmountResource::collection($incomes);
    }

    public function getExpenseRecord()
    {
        $expenses = Expense::where('category_type', '=', 'Expenses')->get();
        return ExpenseResource::collection($expenses);
    }

    public function getIncomeRecord()
    {
        $incomes = Expense::where('category_type', '=', 'Income')->get();
        return ExpenseResource::collection($incomes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $amount = Expense::create($request->all());
        return $amount;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
