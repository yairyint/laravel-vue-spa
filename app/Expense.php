<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = "expenses";
    protected $fillable = ['amount', 'category_id', 'category_type'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
