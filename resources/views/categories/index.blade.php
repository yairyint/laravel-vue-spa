@extends('layouts.app')

@section('navigation')
	<navigation>
	</navigation>
@endsection

@section('content')
	<div class="col-md-12">
		<router-view default="{ name: 'categoriesIndex' }"></router-view>
	</div>
@endsection