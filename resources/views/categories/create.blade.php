@extends('layouts.app')

@section('navigation')
    <div class="col-md-4">
        <navigation></navigation>
    </div>
@endsection

@section('content')
    <div class="col-md-8">
        <form method="POST" action="{{ action('CategoryController@store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" rows="5" class="form-control" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Please Select</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="type">
                          <option value=1>Expenses</option>
                          <option value=2>Income</option>
                        </select>
                    </div>
                    <button class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
@endsection