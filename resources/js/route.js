import Layout from './components/layouts/Layout.vue'

import categories from './components/category/list.vue'
import categoryForm from './components/category/create.vue'
import editCategory from './components/category/edit.vue'
import ExpenseList from './components/expenses/list.vue'
import IncomeList from './components/incomes/list.vue'
import dashboard from './components/dashboard.vue'
import loginForm from './components/authentication/login.vue'
import registerForm from './components/authentication/register.vue'

const router = [
	{
		path : '/',
		component : Layout,
		redirect : '/dashboard',
		children : [
			{
				path: '/api/categories/:id/edit',
				name: 'editCategory',
				component: editCategory,
				props: true,
				
			},
			{
	  			path: '/dashboard',
	  			name: 'dashboard',
	  			component: dashboard,
				props: true,
				meta: {
					auth: true
				}
	  		},
			{
				path: '/categories',
				name: 'categories',
				component: categories,
				meta: {
					auth: true
				}
			},
			{
				path: '/expenses',
				name: 'expenses',
				component: ExpenseList,
				meta: {
					auth: true
				}
			},
			{
				path: '/incomes',
				name: 'incomes',
				component: IncomeList,
				meta: {
					auth: true
				}
			},
			{
				path: '/create',
				name: 'categoryForm',
				component: categoryForm,
				meta: {
					auth: true
				}
			},
			{
				path: '/login',
				name: 'login',
				component: loginForm,
				meta: {
					auth: false
				}
			},
			{
				path: '/register',
				name: 'register',
				component: registerForm,
				meta: {
					auth: false
				}
			}
		]
	}
];

export default router