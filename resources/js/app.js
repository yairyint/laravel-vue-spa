
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import { Notification } from 'element-ui'
import App from './App'
import { routerHistory, writeHistory } from 'vue-router-back-button'
import 'element-ui/lib/theme-chalk/index.css'
import elementLangEn from 'element-ui/lib/locale/lang/en';
import elementLocale from 'element-ui/lib/locale';


elementLocale.use(elementLangEn);
Vue.use(ElementUI)

import routes from './route.js'

const router = new VueRouter({
	history : true,
	routes
})

Vue.use(VueRouter)
Vue.use(routerHistory)
Vue.router=router
Vue.axios=axios

axios.defaults.baseURL = 'http://localhost:8000/api';

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

router.afterEach(writeHistory)

var app = new Vue({
    el: '#app',
    props: ['id'],
    render: h => h(App),
    router,
});

