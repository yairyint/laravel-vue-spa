<?php

use Illuminate\Database\Seeder;

class IncomeCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        App\Category::insert([
            'name' => 'Salary',
            'description' => 'Category',
            'type' => 'Income'
        ]);
        App\Category::insert([
            'name' => 'Awards',
            'description' => 'Category',
            'type' => 'Income'
        ]);
        App\Category::insert([
            'name' => 'Investments',
            'description' => 'Category',
            'type' => 'Income'
        ]);
        App\Category::insert([
            'name' => 'Grants',
            'description' => 'Category',
            'type' => 'Income'
        ]);
        App\Category::insert([
            'name' => 'Coupons',
            'description' => 'Category',
            'type' => 'Income'
        ]);
        App\Category::insert([
            'name' => 'Others',
            'description' => 'Category',
            'type' => 'Income'
        ]);
    }
}
